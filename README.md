# chrono

A simple vuejs chrono pwa

## Libraries used

* vuetify
* vuepress
* workbox

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Serve and preview the builded folder
```
yarn preview
```

### Compiles and minifies for production
```
yarn build
```

### Run your unit tests
```
yarn test:unit
```

### Run your end-to-end tests
```
yarn test:e2e
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


## PWA Icons

Favicon generator : https://realfavicongenerator.net/

Plusieurs possibilités :
 
* https://dev.to/jcalixte/vuejs-pwa-assets-generator-4jhn
* https://www.npmjs.com/package/vue-pwa-asset-generator
* `npx pwa-asset-generator ./src/assets/icon/icon.png --background "#DD00FF" --scrape false` to generate the asset from the icon.
